# Altera USB-Blaster Windows 驱动程序

## 简介

本仓库提供了一个资源文件 `Altera-usb-blaster-windows-drivers.zip`，该文件包含了 Altera USB-Blaster 的 Windows 驱动程序。这些驱动程序与 Quartus Programmer 程序配套使用，支持 Windows 10 操作系统，并且兼容 32 位和 64 位系统。该驱动程序主要用于 Altera CPLD 的升级。

## 文件描述

- **文件名**: `Altera-usb-blaster-windows-drivers.zip`
- **文件内容**: Altera USB-Blaster 的 Windows 驱动程序
- **适用系统**: Windows 10 (32 位和 64 位)
- **用途**: 与 Quartus Programmer 程序配套使用，支持 Altera CPLD 的升级

## 使用方法

1. 下载 `Altera-usb-blaster-windows-drivers.zip` 文件。
2. 解压缩文件到任意目录。
3. 根据操作系统位数（32 位或 64 位）选择相应的驱动程序进行安装。
4. 安装完成后，连接 Altera USB-Blaster 设备，系统将自动识别并安装驱动。

## 注意事项

- 请确保操作系统为 Windows 10，并且系统位数与驱动程序匹配。
- 安装驱动程序前，请关闭所有 Quartus 相关程序。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本项目采用 [MIT 许可证](LICENSE)。